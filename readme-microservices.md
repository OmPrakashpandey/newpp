
# Dev Enviroment setup – NodeJS
> Node.js® is a JavaScript runtime built on Chrome's V8 JavaScript engine.


## Installation
Linux:

Step 1 – Add Node.js PPA


Use LTS Release : At the last update of this tutorial, Node.js 12.x is the LTS release available.
```sh
sudo apt-get install curl
curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash -
```

Step 2 – Install Node.js on Ubuntu
```sh
sudo apt-get install nodejs
```
Step 3 – Check Node.js and NPM Version
```sh
node -v // v12.13.0
```
```sh
npm -v  //6.12.0
```



Install MongoDB on Ubuntu

Import the public key used by the package management system

wget -qO - https://www.mongodb.org/static/pgp/server-4.2.asc | sudo apt-key add -

Create a list file for MongoDB

echo "deb [ arch=amd64 ] https://repo.mongodb.org/apt/ubuntu xenial/mongodb-org/4.2 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-4.2.list

Reload local package database.

sudo apt-get update

Install the MongoDB packages.

sudo apt-get install -y mongodb-org

Start MongoDB

sudo service mongod start

Begin using MongoDB

mongo
